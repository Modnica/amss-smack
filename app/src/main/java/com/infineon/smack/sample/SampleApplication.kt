package com.infineon.smack.sample

import android.annotation.SuppressLint
import android.app.Application
import android.provider.Settings
import com.infineon.smack.sample.logs.TimberRemoteTree
import com.infineon.smack.sample.logs.entities.DeviceInfo
import com.infineon.smack.sdk.application.lock.key.LockKey
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class SampleApplication : Application() {

    @SuppressLint("HardwareIds")
    override fun onCreate() {
        super.onCreate()

        val deviceId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        val deviceDetails = DeviceInfo(deviceId)
        val remoteTree = TimberRemoteTree(deviceDetails)

        Timber.plant(remoteTree)
        AndroidThreeTen.init(this)
    }

    companion object {
        var knownLockKey: LockKey? = null
    }
}
