package com.infineon.smack.sample

import com.infineon.smack.sdk.SmackSdk
import com.infineon.smack.sdk.android.AndroidNfcAdapterWrapper
import com.infineon.smack.sdk.application.lock.LockApi
import com.infineon.smack.sdk.log.AndroidSmackLogger
import com.infineon.smack.sdk.mailbox.datapoint.MailboxDataPoint
import com.infineon.smack.sdk.smack.DefaultSmackClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SmackModule {

    @Singleton
    @Provides
    @Suppress("InjectDispatcher")
    fun provideSmackSdk(): SmackSdk {
        return SmackSdk.Builder()
            .setSmackClient(DefaultSmackClient(AndroidSmackLogger()))
            .setNfcAdapterWrapper(AndroidNfcAdapterWrapper())
            .setCoroutineDispatcher(Dispatchers.IO)
            .build()
    }

    @Singleton
    @Provides
    fun provideLockApi(smackSdk: SmackSdk): LockApi {
        return smackSdk.lockApi
    }

    @Singleton
    @Provides
    fun provideCoroutineDispatcher(smackSdk: SmackSdk): CoroutineDispatcher {
        return smackSdk.coroutineDispatcher
    }
}
