package com.infineon.smack.sample.logs.entities

data class RemoteLog(
    val priority: String,
    val tag: String?,
    val message: String,
    val throwable: String?,
    val time: String
)


