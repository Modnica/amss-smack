package com.infineon.smack.sample.overview

import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.widget.EditText
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.infineon.smack.sample.R
import com.infineon.smack.sample.databinding.MainActivityBinding
import com.infineon.smack.sdk.SmackSdk
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: MainActivityBinding

    @VisibleForTesting
    @Inject
    lateinit var smackSdk: SmackSdk

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.resetButton.setOnClickListener { viewModel.resetCount() }
        binding.firmwareSwitch.setOnCheckedChangeListener { _, checked ->
            viewModel.setFirmwareToggle(checked)
        }
        binding.usernameInput.doOnTextChanged { text, _, _, _ ->
            viewModel.onNameChanged(text.toString())
        }
        binding.supervisorKeyInput.doOnTextChanged { text, _, _, _ ->
            viewModel.onSupervisorKeyChanged(text.toString())
        }
        binding.passwordInput.doOnTextChanged { text, _, _, _ ->
            viewModel.onPasswordChanged(text.toString())
        }
        binding.lockOrUnlockSwitch.setOnClickListener {
            viewModel.onLockSwitchClicked()
        }
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.viewState.collect(::updateViewState)
            }
        }
        smackSdk.onCreate(this)

        handleNfcIntent(intent)
    }

    private fun showAlert(
        @StringRes titleRes: Int,
        @StringRes messageRes: Int,
        exceptionMessage: String? = null
    ) {
        smackSdk.isEnabled = false
        runOnUiThread {
            AlertDialog.Builder(this)
                .setTitle(titleRes)
                .setMessage("${getString(messageRes)}\n\"$exceptionMessage\"")
                .setNeutralButton(R.string.ok) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
                .setOnDismissListener {
                    viewModel.resetException()
                    smackSdk.isEnabled = true
                }
                .show()
        }
    }


    public override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleNfcIntent(intent)
    }

    private fun handleNfcIntent(intent: Intent?) {
        logIntent(intent)
//        if (intent?.action == NfcAdapter.ACTION_TAG_DISCOVERED) {
//            Timber.i("Handle nfc intent")
        if (intent?.extras != null) { smackSdk.onNewIntent(intent) }
//        }
    }

    private fun Intent.formattedExtras(): String {
        return extras
            ?.run {
                keySet()
                    .associateWith {
                        get(it).let { value ->
                            if (value is Parcelable) {
                                value.describeContents()
                            } else {
                                value
                            }
                        }
                    }
                    .map { it.run { "$key: \"$value\"" } }
            }
            ?.joinToString(separator = ", ")
            .toString()
            .let { "$action $it" }
    }

    private fun Intent?.formattedAction(): String {
        return "No extras intent: ${this?.action.toString()}"
    }

    private fun logIntent(intent: Intent?) {
        if (intent?.extras != null) {
            Timber.i(message = intent.formattedExtras())
        } else {
            Timber.i(message = intent.formattedAction())
        }
    }

    private fun updateViewState(state: MainViewState) = with(binding) {
        showConnection(state.isConnected, state.lockId)
        sentValueTextView.text = state.sentBytesCount.toString()
        receivedValueTextView.text = state.receivedBytesCount.toString()
        divergentValueTextView.text = state.divergentBytesCount.toString()
        firmwareText.isVisible = state.showMissingFirmwareText

        usernameInput.setInput(state.userName)
        supervisorKeyInput.setInput(state.supervisorKey)
        passwordInput.setInput(state.password)

        lockOrUnlockText.text = if (state.isLockState) "Lock" else "Unlock"
        lockOrUnlockSwitch.isChecked = state.isLockState

        if (state.alertTitle != null && state.alertMessage != null) {
            showAlert(state.alertTitle, state.alertMessage, state.exceptionMessage)
        }
    }

    private fun EditText.setInput(newInput: String?) {
        val input = newInput ?: ""
        setText(input)
        setSelection(input.length)
    }

    private fun MainActivityBinding.showConnection(isConnected: Boolean, lockId: Long?) {
        connectedImageView.isVisible = isConnected
        connectedTextView.isVisible = isConnected
        disconnectedImageView.isVisible = !isConnected
        disconnectedTextView.isVisible = !isConnected

        usernameInput.isVisible = !isConnected
        supervisorKeyInput.isVisible = !isConnected
        passwordInput.isVisible = !isConnected

        if (isConnected && lockId != null) {
            val connectedInfoWithId = "${getString(R.string.nfc_connected)} to lock with id: $lockId"
            connectedTextView.text = connectedInfoWithId
        }
    }
}
