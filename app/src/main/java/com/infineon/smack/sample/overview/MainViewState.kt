package com.infineon.smack.sample.overview

import androidx.annotation.StringRes

data class MainViewState(
    val isConnected: Boolean = false,
    val lockId: Long? = null,
    val sentBytesCount: Long = 0,
    val receivedBytesCount: Long = 0,
    val divergentBytesCount: Long = 0,
    val lockKey: String = "",
    @StringRes val alertTitle: Int? = null,
    @StringRes val alertMessage: Int? = null,
    val exceptionMessage: String? = null,
    val showMissingFirmwareText: Boolean = false,
    val userName: String = "DefaultUserName",
    val supervisorKey: String? = null,
    val password: String? = null,
    val isLockState: Boolean = false
)
