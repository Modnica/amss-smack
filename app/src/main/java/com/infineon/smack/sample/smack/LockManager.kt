package com.infineon.smack.sample.smack

import com.infineon.smack.sdk.application.lock.Lock
import com.infineon.smack.sdk.application.lock.LockApi
import com.infineon.smack.sdk.application.lock.key.LockKey
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LockManager @Inject constructor(
    private val lockApi: LockApi,
    // maybe use Dispatchers.IO if this is main thread
    private val coroutineDispatcher: CoroutineDispatcher
) {

    fun getLock(): Flow<Lock?> = lockApi.getLock()

    suspend fun setLockKey(
        lock: Lock,
        userName: String,
        supervisorKey: String,
        password: String,
        dateTimeInSeconds: Long
    ): LockKey = withContext(coroutineDispatcher) {
        lockApi.setLockKey(
            lock = lock,
            userName = userName,
            supervisorKey = supervisorKey,
            password = password,
            dateTimeInSeconds = dateTimeInSeconds
        )
    }

    suspend fun validatePassword(
        lock: Lock,
        userName: String,
        password: String,
        dateTimeInSeconds: Long
    ): LockKey = withContext(coroutineDispatcher) {
        lockApi.validatePassword(
            lock = lock,
            userName = userName,
            dateTimeInSeconds = dateTimeInSeconds,
            password = password
        )
    }

    suspend fun initializeSession(
        lock: Lock,
        userName: String,
        lockKey: LockKey,
        dateTimeInSeconds: Long
    ) = withContext(coroutineDispatcher) {
        lockApi.initializeSession(
            lock = lock,
            userName = userName,
            dateTimeInSeconds = dateTimeInSeconds,
            lockKey = lockKey
        )
    }

    suspend fun lock(lock: Lock, lockKey: LockKey) = withContext(coroutineDispatcher) {
        lockApi.lock(lock = lock, lockKey = lockKey)
    }

    suspend fun unlock(lock: Lock, lockKey: LockKey) = withContext(coroutineDispatcher) {
        lockApi.unlock(lock = lock, lockKey = lockKey)
    }
}
