package com.infineon.smack.sample.overview

import androidx.annotation.StringRes
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.infineon.smack.sample.R
import com.infineon.smack.sample.SampleApplication
import com.infineon.smack.sample.smack.LockManager
import com.infineon.smack.sample.smack.SampleSmackApi
import com.infineon.smack.sample.smack.SampleSmackState
import com.infineon.smack.sdk.application.lock.Lock
import com.infineon.smack.sdk.mailbox.exception.WrongKeyException
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    coroutineDispatcher: CoroutineDispatcher,
    private val smackApi: SampleSmackApi,
    private val lockManager: LockManager,
) : ViewModel() {

    @VisibleForTesting
    private val _viewState = MutableStateFlow(MainViewState())

    val viewState: StateFlow<MainViewState> = _viewState.asStateFlow()

    init {
        viewModelScope.launch {
            lockManager.getLock()
                .catch {
                    // tag was lost
                    handleException(it)
                }
                // drop first because of error if lock is null (always null after app launch)
//                .drop(1)
                .collect { lock ->
                    val state = viewState.value
                    if (lock != null) {
                        val knownLockKey = /*getLockKeyFromDatabase(lock)*/
                            SampleApplication.knownLockKey
                        if (knownLockKey != null) {
                            try {
                                lockManager.initializeSession(
                                    lock,
                                    state.userName,
                                    knownLockKey,
                                    System.currentTimeMillis() / 1000
                                )
                                if (state.isLockState) {
                                    Timber.i("trying to lock $lock")
                                    lockManager.lock(lock, knownLockKey)
                                    Timber.i("Was locked $lock!")
                                } else {
                                    Timber.i("trying to unlock $lock")
                                    lockManager.unlock(lock, knownLockKey)
                                    Timber.i("Was unlocked $lock!")
                                }
                            } catch (exception: Exception) {
                                handleException(exception)
                            }
                        } else {
                            Timber.i("Trying to connect $lock")
                            Timber.d(message = "username: ${state.userName}, pass: ${state.password}, key: ${state.supervisorKey}")
                            if (lock.isNew) {
                                // Show a register screen to setup a brand new lock (See example below)
                                registerLock(lock)
                            } else {
                                // Show a login screen to setup a lock that was already registered before (S
                                validateLockPassword(lock)
                            }
                        }
                    } else {
                        Timber.d("Lock is null.")
                    }
                }
        }
        viewModelScope.launch {
            smackApi
                .getStateAndKeepAlive()
                .onEach { it.setToViewState() }
                .flowOn(coroutineDispatcher)
                .catch { exception ->
                    handleException(exception)
                    throw exception
                }
                .retry { exception ->
                    exception !is CancellationException
                }
        }
    }

    private fun SampleSmackState.setToViewState(): MainViewState {
        return MainViewState(
            isConnected = isConnected,
            sentBytesCount = byteCount.sentByteCount,
            receivedBytesCount = byteCount.receivedByteCount,
            divergentBytesCount = byteCount.divergentByteCount,
            showMissingFirmwareText = shouldShowMissingFirmware
        )
    }

    private fun handleException(throwable: Throwable) {
        setState {
            copy(
                alertTitle = getErrorTitleResId(throwable),
                alertMessage = getErrorMessageResId(throwable),
                exceptionMessage = throwable.message,
                showMissingFirmwareText = smackApi.firmwareToggleFlow.value
            )
        }
        Timber.e(throwable)
    }

    fun resetException() {
        setState { copy(alertTitle = null, alertMessage = null, exceptionMessage = null) }
    }

    fun handleNfc() {
        viewModelScope.launch {

        }
    }

    private fun registerLock(lock: Lock) {
        Timber.d("Lock start registration $lock")
        viewModelScope.launch {
            with(viewState.value) {
                if (supervisorKey.isNullOrBlank()) {
                    handleException(Exception("Supervisor key is necessary for registration"))
                    return@with
                }
                if (password.isNullOrBlank()) {
                    handleException(Exception("Password is necessary for registration"))
                    return@with
                }
                if (lock.isNew) {
                    try {
                        val lockKey = lockManager.setLockKey(
                            lock,
                            userName,
                            supervisorKey,
                            password,
                            LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)
                        )
                        // Store the lock key for this lock, for further usage.
//                        saveLockKeyToDatabase(lock, lockKey)
                        Timber.i("Lock was registered $lock")
                        SampleApplication.knownLockKey = lockKey
                        setConnectedState(lock.lockId)
                    } catch (exception: Exception) {
                        handleException(exception)
                    }
                }
            }
        }
    }

    private fun validateLockPassword(lock: Lock) {
        Timber.d("Lock start validation $lock")
        viewModelScope.launch {
            with(viewState.value) {
                if (password.isNullOrBlank()) {
                    handleException(Exception("Password is necessary for session"))
                    return@launch
                }
                if (!lock.isNew) {
                    try {
                        val lockKey = lockManager.validatePassword(
                            lock,
                            userName,
                            password,
                            System.currentTimeMillis() / 1000
                        )
                        Timber.i("Lock was validated $lock")
                        SampleApplication.knownLockKey = lockKey
                        setConnectedState(lock.lockId)
//                    saveLockKeyToDatabase(lock, lockKey)
                    } catch (exception: WrongKeyException) {
                        handleException(exception)
                    }
                }
            }
        }
    }

    fun onNameChanged(newName: String) {
        setState { copy(userName = newName) }
    }

    fun onSupervisorKeyChanged(newKey: String) {
        setState { copy(supervisorKey = newKey.ifBlank { null }) }
    }

    fun onPasswordChanged(newPassword: String) {
        setState { copy(password = newPassword.ifBlank { null }) }
    }

    fun resetCount() {
        smackApi.resetCount()
    }

    fun setFirmwareToggle(enabled: Boolean) {
        smackApi.setFirmwareToggle(enabled)
    }

    fun onLockSwitchClicked() {
        setState { copy(isLockState = !isLockState) }
    }

    private fun setState(transform: MainViewState.() -> MainViewState) {
        _viewState.value = _viewState.value.transform()
    }

    private fun setConnectedState(lockId: Long?) {
        setState { copy(isConnected = true, lockId = lockId) }
    }

    @StringRes
    private fun getErrorTitleResId(exception: Throwable): Int =
        when (isWrongKeyException(exception)) {
            true -> R.string.error_title_invalid_key
            else -> R.string.error_title_try_again
        }

    @StringRes
    private fun getErrorMessageResId(exception: Throwable): Int =
        when (isWrongKeyException(exception)) {
            true -> R.string.error_message_invalid_key
            else -> R.string.error_message_try_again
        }

    private fun isWrongKeyException(exception: Throwable) = exception is WrongKeyException
}
